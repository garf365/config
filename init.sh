#!/bin/bash

git submodule update --init

packages=" \
  bmon \
  curl \
	emacs \
  galculator \
	git \
	gpm \
  htop \
  iotop \
  iptraf \
  mutt \
	python3-dev \
	python3-pip \
  ipython \
	stow \
	terminator \
  thefuck \
	vim"

stowed="bash terminator vim emacs git mutt"

# install basic tools
sudo apt install -y $packages

# update pip
pip3 install --upgrade pip
# install "the fuck" command
pip3 install thefuck

# customize config script for bash and vim
for app in ${stowed}
do
  cd ${app}
  find . -type f | while read f
  do
    rm ~/${f} 2>/dev/null
  done
  cd ..
  stow ${app}
done


